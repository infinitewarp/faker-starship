"""Setup script for faker-starship."""

from setuptools import find_packages, setup

with open("README.md", "r") as f:
    long_description = f.read()

setup(
    name="faker-starship",
    version="0.0.1",
    author="Brad Smith",
    author_email="bradster@infinitewarp.com",
    description="Provider of various starship data for the Faker Python package.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    keywords="faker provider starship test data",
    license="MIT",
    url="https://gitlab.com/infinitewarp/faker-starship",
    packages=find_packages(),
    install_requires=["faker"],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Topic :: Software Development",
        "Topic :: Software Development :: Libraries",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
)
