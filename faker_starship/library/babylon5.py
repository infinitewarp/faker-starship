# -*- coding: utf-8 -*-
"""Starship data from Babylon 5 franchise."""

from __future__ import unicode_literals

names = {
    "Achilles",
    "Archimedes",
    "Arkati",
    "Asimov",
    "Babylon 5",
    "Carbo",
    "Crystal Cavern",
    "Dionysus",
    "Dogato",
    "Drala Fi",
    "Dyson",
    "EAS Acheron",
    "EAS Aegean",
    "EAS Agamemnon",
    "EAS Agrippa",
    "EAS Alexander",
    "EAS Amundsen",
    "EAS Anubis",
    "EAS Apollo",
    "EAS Ares",
    "EAS Artemis",
    "EAS Athena",
    "EAS Cadmus",
    "EAS Cerberus",
    "EAS Charon",
    "EAS Churchill",
    "EAS Clarkstown",
    "EAS Cortez",
    "EAS Curie",
    "EAS Damocles",
    "EAS De Soto",
    "EAS Delphi",
    "EAS Eratosthenes",
    "EAS Euripides",
    "EAS Excalibur",
    "EAS Foxfire",
    "EAS Furies",
    "EAS Galatea",
    "EAS Heracles",
    "EAS Hermes",
    "EAS Hydra",
    "EAS Hyperion",
    "EAS Juno",
    "EAS Leviathan",
    "EAS Lexington",
    "EAS Medusa",
    "EAS Nemesis",
    "EAS Nimrod",
    "EAS Olympic",
    "EAS Omega",
    "EAS Orion",
    "EAS Persephone",
    "EAS Pollux",
    "EAS Pournelle",
    "EAS Prometheus",
    "EAS Roanoke",
    "EAS Schwarzkopf",
    "EAS Talos",
    "EAS Theseus",
    "EAS Titans",
    "EAS Vesta",
    "Earthforce One",
    "Earthforce Two",
    "Enfali",
    "Enfili",
    "Excalibur",
    "G'Tok",
    "IAS Excalibur",
    "Icarus",
    "Ingata",
    "Interstellar Alliance One",
    "Kar'ti",
    "Khatkhata",
    "Liandra",
    "Lumini",
    "Marie Celeste",
    "Molios",
    "Na'Tan",
    "Na'Toth",
    "Olympus",
    "Qualthaa",
    "Red Star 9",
    "Salvage One",
    "Skydancer",
    "Solaris",
    "Strakath",
    "Takari",
    "Tal’Quith",
    "Toreador",
    "Trigati",
    "USS Copernicus",
    "Ulysses",
    "Valen",
    "Valerius",
    "Vancori",
    "Vellurian",
    "Victory",
    "White Star",
    "White Star 1",
    "White Star 12",
    "White Star 14",
    "White Star 16",
    "White Star 2",
    "White Star 26",
    "White Star 27",
    "White Star 43",
    "White Star 7",
    "White Star 9",
    "White Star 90",
    "White Star Prime",
    "Wolfbane",
}

classes = {
    "Delta Gamma 9",
    "Explorer",
    "Frazi",
    "G'Quan",
    "Hyperion",
    "Nial",
    "Nova",
    "Olympus",
    "Omega",
    "Primus",
    "Sentri",
    "Sharlin",
    "Starfury",
    "Sun Hawk",
    "T'Loth",
    "Valen",
    "Victory",
    "Vorchan",
    "White Star",
}
