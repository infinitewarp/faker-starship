# -*- coding: utf-8 -*-
"""Starship data from Lexx franchise."""

from __future__ import unicode_literals

names = {"Foreshadow", "Lexx", "Little Lexx", "Megashadow"}

classes = {"Moth"}
