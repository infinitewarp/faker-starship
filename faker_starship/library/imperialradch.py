# -*- coding: utf-8 -*-
"""Starship data from Imperial Radch franchise (Ancillary Justice, et al)."""

from __future__ import unicode_literals

names = {
    "Justice of Ente ",
    "Justice of Toren",
    "Mercy of Ilves",
    "Mercy of Kalr",
    "Mercy of Phey",
    "Mercy of Sarrse",
    "Sword of Amaat",
    "Sword of Atagaris",
    "Sword of Gurat ",
    "Sword of Inil",
    "Sword of Nathtas",
    "Sword of Tlen",
}
