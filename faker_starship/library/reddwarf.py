# -*- coding: utf-8 -*-
"""Starship data from Red Dwarf franchise."""

from __future__ import unicode_literals

names = {
    "SS Augustus",
    "Blue Midget",
    "SS Esperanto",
    "Gelf Battle Cruiser",
    "Holoship",
    "Nova 5",
    "SS Oregon",
    "Red Dwarf",
    "Starbug",
}
