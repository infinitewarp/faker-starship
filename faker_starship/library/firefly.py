# -*- coding: utf-8 -*-
"""Starship data from Firefly franchise."""

from __future__ import unicode_literals

names = {
    "Brutus",
    "CanTankerous",
    "Haphazard",
    "Huntingdon's Bolt",
    "I.A.V. Alexander",
    "I.A.V. Cortez",
    "I.A.V. Dortmunder",
    "I.A.V. Magellan",
    "I.A.V. Stormfront",
    "OddEasy",
    "Rascal Puff",
    "SS Walden",
    "Serenity",
    "White Lightning",
}

classes = {
    "Ark",
    "Arrowhead",
    "Bernard",
    "CL-54 Mogushchestvenniy",
    "Carrion House",
    "Fenris",
    "Firefly",
    "Glory",
    "Hanover",
    "Iliad",
    "Kepler",
    "Knorr",
    "Komodo",
    "Longbow",
    "Odyssey",
    "Orion",
    "Paradise",
    "Peregrin",
    "Pilgrim",
    "Sampan",
    "Sandfly",
    "Shu Fu",
    "Thunderbird",
    "Titan",
    "Tohoku",
    "Trans-U",
    "Vanderdecken",
    "Victoria",
}
