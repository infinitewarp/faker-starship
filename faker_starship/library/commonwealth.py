# -*- coding: utf-8 -*-
"""Starship data from Commonwealth and related Peter F. Hamilton works."""

from __future__ import unicode_literals

names = {
    "Aeolus",
    "Baghdad",
    "Cairo",
    "Conway",
    "Dauntless",
    "Defender",
    "Defiant",
    "Desperado",
    "Dublin",
    "Galibi",
    "Langharne",
    "London",
    "Moscow",
    "Saumarez",
    "Searcher",
    "Second Chance",
    "Speedwell",
    "StAsaph",
    "Tokyo",
}
