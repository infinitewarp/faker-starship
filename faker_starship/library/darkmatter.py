# -*- coding: utf-8 -*-
"""Starship data from Dark Matter franchise."""

from __future__ import unicode_literals

names = {
    "Arcturus-4",
    "Balda-9",
    "Cygni-4",
    "EOS-7",
    "Eridani-6",
    "FCS Condor",
    "FCS Deliverance",
    "Hyadum-12",
    "ISS Far Horizon",
    "Janah-12",
    "MCS Murakami",
    "MCS Sujin",
    "Ophiuchus",
    "Raza",
    "Regulus-12",
    "Shaofu-2",
    "Taliphus-8",
    "The Marauder",
    "VRC Dimitriev",
    "Vega-5",
}

classes = {"Carrier", "Cruiser", "Destroyer", "Phantom", "Shuttle", "Tribal"}
