# -*- coding: utf-8 -*-
"""Starship data from The Orville franchise."""

from __future__ import unicode_literals

names = {
    "Kakov",
    "USS Blériot",
    "USS Burnell",
    "USS Chanute",
    "USS Clemens",
    "USS Druyan",
    "USS Hawking",
    "USS Olympia",
    "USS Orville",
    "USS Quimby",
    "USS Roosevelt",
    "USS Spruance",
    "USS Watson",
    "Yakar",
}

classes = {
    "Colossus",
    "Cruiser",
    "Destroyer",
    "Exploratory",
    "Fighter",
    "Interceptor",
    "Leviathan",
    "Marauder",
    "Science",
    "Transport",
}

registries = {
    "ECV-197",
    "ECV-197-1",
    "ECV-197-2",
    "ECV-197-4",
    "ECV-342",
    "ECV-342-1",
    "LCV-519",
    "LCV-529",
    "SCV-183",
    "SCV-183-2",
}
