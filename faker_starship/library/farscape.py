# -*- coding: utf-8 -*-
"""Starship data from Farscape franchise."""

from __future__ import unicode_literals

names = {
    "Agrona",
    "Challenger",
    "Cilla",
    "Collaroy",
    "Decimator",
    "Dorvala",
    "Elack",
    "Farscape-1",
    "Halos-1",
    "Lo'La",
    "Moya",
    "Penetrator",
    "Rogue",
    "Rovhu",
    "Talyn",
    "Tiyo",
    "Undaunted",
    "Zelbinion",
}

classes = {
    "Dreadnought",
    "Leviathan",
    "Marauder",
    "Predator",
    "Prowler",
    "Stryker",
    "Vigilante",
}
