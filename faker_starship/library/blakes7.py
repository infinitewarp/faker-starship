# -*- coding: utf-8 -*-
"""Starship data from Blake's 7 franchise."""

from __future__ import unicode_literals

names = {"London", "Liberator", "Scorpio", "Deep Space Vehicle 2"}

classes = {"Wanderer", "DSV"}

registrations = {"K47"}
