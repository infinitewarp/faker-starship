"""Tests for faker_starship."""

import os
import sys

import pytest
import six
from faker import Faker

local_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(local_path, ".."))
from faker_starship import Provider as StarshipProvider  # noqa: E402


@pytest.fixture(scope="module")
def fake():
    """Fixture of Faker instance with StarshipProvider added."""
    fixture = Faker()
    fixture.add_provider(StarshipProvider)
    return fixture


def assert_not_empty_string(value):
    """Assert the input value is a not-empty string."""
    assert value is not None
    assert isinstance(value, six.string_types)
    assert len(value) > 0


def test_starship_name(fake):
    """Test starship_name with no args."""
    starship_name = fake.starship_name()
    assert_not_empty_string(starship_name)


def test_starship_name_from_valid_source(fake):
    """Test starship_name with a valid source arg."""
    starship_name = fake.starship_name("startrek")
    assert_not_empty_string(starship_name)


def test_starship_class(fake):
    """Test starship_class with no args."""
    starship_class = fake.starship_class()
    assert_not_empty_string(starship_class)


def test_starship_class_from_valid_source(fake):
    """Test starship_class with a valid source arg."""
    starship_class = fake.starship_class("startrek")
    assert_not_empty_string(starship_class)


def test_starship_registry(fake):
    """Test starship_registry with no args."""
    starship_registry = fake.starship_registry()
    assert_not_empty_string(starship_registry)


def test_starship_registry_from_valid_source(fake):
    """Test starship_registry with a valid source arg."""
    starship_registry = fake.starship_registry("startrek")
    assert_not_empty_string(starship_registry)
